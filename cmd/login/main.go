package main

import (
	"context"
	"errors"
	"log"
	"net"
	"net/http"
	"time"

	"github.com/kelseyhightower/envconfig"
	"github.com/rs/cors"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	goahttp "goa.design/goa/v3/http"
	goa "goa.design/goa/v3/pkg"
	"golang.org/x/sync/errgroup"

	"gitlab.eclipse.org/eclipse/xfsc/tsa/golib/graceful"
	goahealth "gitlab.eclipse.org/eclipse/xfsc/tsa/login/gen/health"
	goahealthsrv "gitlab.eclipse.org/eclipse/xfsc/tsa/login/gen/http/health/server"
	goaloginsrv "gitlab.eclipse.org/eclipse/xfsc/tsa/login/gen/http/login/server"
	goaopenapisrv "gitlab.eclipse.org/eclipse/xfsc/tsa/login/gen/http/openapi/server"
	goalogin "gitlab.eclipse.org/eclipse/xfsc/tsa/login/gen/login"
	"gitlab.eclipse.org/eclipse/xfsc/tsa/login/gen/openapi"
	"gitlab.eclipse.org/eclipse/xfsc/tsa/login/internal/clients/email"
	"gitlab.eclipse.org/eclipse/xfsc/tsa/login/internal/clients/ocm"
	"gitlab.eclipse.org/eclipse/xfsc/tsa/login/internal/clients/policy"
	"gitlab.eclipse.org/eclipse/xfsc/tsa/login/internal/config"
	"gitlab.eclipse.org/eclipse/xfsc/tsa/login/internal/service"
	"gitlab.eclipse.org/eclipse/xfsc/tsa/login/internal/service/health"
	"gitlab.eclipse.org/eclipse/xfsc/tsa/login/internal/service/login"
	"gitlab.eclipse.org/eclipse/xfsc/tsa/login/internal/token"
)

var Version = "0.0.0+development"

func main() {
	var cfg config.Config
	if err := envconfig.Process("", &cfg); err != nil {
		log.Fatalf("cannot load configuration: %v\n", err)
	}

	logger, err := createLogger(cfg.LogLevel)
	if err != nil {
		log.Fatalln(err)
	}
	defer logger.Sync() //nolint:errcheck

	logger.Info("login service started", zap.String("version", Version), zap.String("goa", goa.Version()))

	httpClient := httpClient()

	policy := policy.New(cfg.Policy.Addr, cfg.Policy.LoginPolicyPath, policy.WithHTTPClient(httpClient))

	token, err := token.New(
		cfg.Token.PublicKey,
		cfg.Token.PrivateKey,
		cfg.Token.Issuer,
		cfg.Token.Audience,
		cfg.Token.Expiration,
	)
	if err != nil {
		logger.Fatal("failed to create token handler", zap.Error(err))
	}

	email, err := email.New(cfg.Mail.Addr, cfg.Mail.User, cfg.Mail.Pass, cfg.Mail.From, cfg.Mail.Subject)
	if err != nil {
		logger.Fatal("error creating mail client", zap.Error(err))
	}

	ocm := ocm.New(cfg.OCM.Addr, httpClient)

	var (
		loginSvc  goalogin.Service
		healthSvc goahealth.Service
	)
	{
		healthSvc = health.New()
		loginSvc = login.New(
			policy, token, email, ocm,
			cfg.LinkLocation, cfg.OCMPollInterval, cfg.OCMPollTimeout,
			cfg.OCM.LoginCredSchemaID, cfg.OCM.LoginCredDefID, logger,
		)
	}

	var (
		loginEndpoints   *goalogin.Endpoints
		healthEndpoints  *goahealth.Endpoints
		openapiEndpoints *openapi.Endpoints
	)
	{
		loginEndpoints = goalogin.NewEndpoints(loginSvc)
		healthEndpoints = goahealth.NewEndpoints(healthSvc)
		openapiEndpoints = openapi.NewEndpoints(nil)
	}

	mux := goahttp.NewMuxer()

	var (
		loginServer   *goaloginsrv.Server
		healthServer  *goahealthsrv.Server
		openapiServer *goaopenapisrv.Server
	)
	{
		loginServer = goaloginsrv.New(loginEndpoints, mux, goahttp.RequestDecoder, goahttp.ResponseEncoder, nil, errFormatter)
		healthServer = goahealthsrv.New(healthEndpoints, mux, goahttp.RequestDecoder, goahttp.ResponseEncoder, nil, errFormatter)
		openapiServer = goaopenapisrv.New(openapiEndpoints, mux, goahttp.RequestDecoder, goahttp.ResponseEncoder, nil, errFormatter, nil, nil)
	}

	goaloginsrv.Mount(mux, loginServer)
	goahealthsrv.Mount(mux, healthServer)
	goaopenapisrv.Mount(mux, openapiServer)

	var handler http.Handler = mux
	if len(cfg.AllowedOrigins) > 0 {
		c := cors.New(cors.Options{AllowedOrigins: cfg.AllowedOrigins})
		handler = c.Handler(mux)
	}

	srv := &http.Server{
		Addr:         cfg.HTTP.Host + ":" + cfg.HTTP.Port,
		Handler:      handler,
		IdleTimeout:  cfg.HTTP.IdleTimeout,
		ReadTimeout:  cfg.HTTP.ReadTimeout,
		WriteTimeout: cfg.HTTP.WriteTimeout,
	}

	g, ctx := errgroup.WithContext(context.Background())
	g.Go(func() error {
		if err := graceful.Shutdown(ctx, srv, 20*time.Second); err != nil {
			logger.Error("server shutdown error", zap.Error(err))
			return err
		}
		return errors.New("server stopped successfully")
	})
	if err := g.Wait(); err != nil {
		logger.Error("run group stopped", zap.Error(err))
	}

	logger.Info("bye bye")
}

func createLogger(logLevel string, opts ...zap.Option) (*zap.Logger, error) {
	var level = zapcore.InfoLevel
	if logLevel != "" {
		err := level.UnmarshalText([]byte(logLevel))
		if err != nil {
			return nil, err
		}
	}

	config := zap.NewProductionConfig()
	config.Level = zap.NewAtomicLevelAt(level)
	config.DisableStacktrace = true
	config.EncoderConfig.TimeKey = "ts"
	config.EncoderConfig.EncodeTime = zapcore.RFC3339TimeEncoder
	return config.Build(opts...)
}

func errFormatter(ctx context.Context, e error) goahttp.Statuser {
	return service.NewErrorResponse(ctx, e)
}

func httpClient() *http.Client {
	return &http.Client{
		Transport: &http.Transport{
			Proxy: http.ProxyFromEnvironment,
			DialContext: (&net.Dialer{
				Timeout: 5 * time.Second,
			}).DialContext,
			MaxIdleConns:        20,
			MaxIdleConnsPerHost: 20,
			TLSHandshakeTimeout: 5 * time.Second,
			IdleConnTimeout:     60 * time.Second,
		},
		Timeout: 10 * time.Second,
	}
}
