// Code generated by goa v3.12.3, DO NOT EDIT.
//
// login HTTP client CLI support package
//
// Command:
// $ goa gen gitlab.eclipse.org/eclipse/xfsc/tsa/login/design

package cli

import (
	"flag"
	"fmt"
	"net/http"
	"os"

	healthc "gitlab.eclipse.org/eclipse/xfsc/tsa/login/gen/http/health/client"
	loginc "gitlab.eclipse.org/eclipse/xfsc/tsa/login/gen/http/login/client"
	goahttp "goa.design/goa/v3/http"
	goa "goa.design/goa/v3/pkg"
)

// UsageCommands returns the set of commands and sub-commands using the format
//
//	command (subcommand1|subcommand2|...)
func UsageCommands() string {
	return `health (liveness|readiness)
login (login|login-invitation|login-with-vc|keys)
`
}

// UsageExamples produces an example of a valid invocation of the CLI tool.
func UsageExamples() string {
	return os.Args[0] + ` health liveness` + "\n" +
		os.Args[0] + ` login login --body '{
      "email": "ima@streichkuhic.com"
   }'` + "\n" +
		""
}

// ParseEndpoint returns the endpoint and payload as specified on the command
// line.
func ParseEndpoint(
	scheme, host string,
	doer goahttp.Doer,
	enc func(*http.Request) goahttp.Encoder,
	dec func(*http.Response) goahttp.Decoder,
	restore bool,
) (goa.Endpoint, any, error) {
	var (
		healthFlags = flag.NewFlagSet("health", flag.ContinueOnError)

		healthLivenessFlags = flag.NewFlagSet("liveness", flag.ExitOnError)

		healthReadinessFlags = flag.NewFlagSet("readiness", flag.ExitOnError)

		loginFlags = flag.NewFlagSet("login", flag.ContinueOnError)

		loginLoginFlags    = flag.NewFlagSet("login", flag.ExitOnError)
		loginLoginBodyFlag = loginLoginFlags.String("body", "REQUIRED", "")

		loginLoginInvitationFlags = flag.NewFlagSet("login-invitation", flag.ExitOnError)

		loginLoginWithVCFlags    = flag.NewFlagSet("login-with-vc", flag.ExitOnError)
		loginLoginWithVCBodyFlag = loginLoginWithVCFlags.String("body", "REQUIRED", "")

		loginKeysFlags = flag.NewFlagSet("keys", flag.ExitOnError)
	)
	healthFlags.Usage = healthUsage
	healthLivenessFlags.Usage = healthLivenessUsage
	healthReadinessFlags.Usage = healthReadinessUsage

	loginFlags.Usage = loginUsage
	loginLoginFlags.Usage = loginLoginUsage
	loginLoginInvitationFlags.Usage = loginLoginInvitationUsage
	loginLoginWithVCFlags.Usage = loginLoginWithVCUsage
	loginKeysFlags.Usage = loginKeysUsage

	if err := flag.CommandLine.Parse(os.Args[1:]); err != nil {
		return nil, nil, err
	}

	if flag.NArg() < 2 { // two non flag args are required: SERVICE and ENDPOINT (aka COMMAND)
		return nil, nil, fmt.Errorf("not enough arguments")
	}

	var (
		svcn string
		svcf *flag.FlagSet
	)
	{
		svcn = flag.Arg(0)
		switch svcn {
		case "health":
			svcf = healthFlags
		case "login":
			svcf = loginFlags
		default:
			return nil, nil, fmt.Errorf("unknown service %q", svcn)
		}
	}
	if err := svcf.Parse(flag.Args()[1:]); err != nil {
		return nil, nil, err
	}

	var (
		epn string
		epf *flag.FlagSet
	)
	{
		epn = svcf.Arg(0)
		switch svcn {
		case "health":
			switch epn {
			case "liveness":
				epf = healthLivenessFlags

			case "readiness":
				epf = healthReadinessFlags

			}

		case "login":
			switch epn {
			case "login":
				epf = loginLoginFlags

			case "login-invitation":
				epf = loginLoginInvitationFlags

			case "login-with-vc":
				epf = loginLoginWithVCFlags

			case "keys":
				epf = loginKeysFlags

			}

		}
	}
	if epf == nil {
		return nil, nil, fmt.Errorf("unknown %q endpoint %q", svcn, epn)
	}

	// Parse endpoint flags if any
	if svcf.NArg() > 1 {
		if err := epf.Parse(svcf.Args()[1:]); err != nil {
			return nil, nil, err
		}
	}

	var (
		data     any
		endpoint goa.Endpoint
		err      error
	)
	{
		switch svcn {
		case "health":
			c := healthc.NewClient(scheme, host, doer, enc, dec, restore)
			switch epn {
			case "liveness":
				endpoint = c.Liveness()
				data = nil
			case "readiness":
				endpoint = c.Readiness()
				data = nil
			}
		case "login":
			c := loginc.NewClient(scheme, host, doer, enc, dec, restore)
			switch epn {
			case "login":
				endpoint = c.Login()
				data, err = loginc.BuildLoginPayload(*loginLoginBodyFlag)
			case "login-invitation":
				endpoint = c.LoginInvitation()
				data = nil
			case "login-with-vc":
				endpoint = c.LoginWithVC()
				data, err = loginc.BuildLoginWithVCPayload(*loginLoginWithVCBodyFlag)
			case "keys":
				endpoint = c.Keys()
				data = nil
			}
		}
	}
	if err != nil {
		return nil, nil, err
	}

	return endpoint, data, nil
}

// healthUsage displays the usage of the health command and its subcommands.
func healthUsage() {
	fmt.Fprintf(os.Stderr, `Health service provides health check endpoints.
Usage:
    %[1]s [globalflags] health COMMAND [flags]

COMMAND:
    liveness: Liveness implements Liveness.
    readiness: Readiness implements Readiness.

Additional help:
    %[1]s health COMMAND --help
`, os.Args[0])
}
func healthLivenessUsage() {
	fmt.Fprintf(os.Stderr, `%[1]s [flags] health liveness

Liveness implements Liveness.

Example:
    %[1]s health liveness
`, os.Args[0])
}

func healthReadinessUsage() {
	fmt.Fprintf(os.Stderr, `%[1]s [flags] health readiness

Readiness implements Readiness.

Example:
    %[1]s health readiness
`, os.Args[0])
}

// loginUsage displays the usage of the login command and its subcommands.
func loginUsage() {
	fmt.Fprintf(os.Stderr, `Login Service enables authorization for user
Usage:
    %[1]s [globalflags] login COMMAND [flags]

COMMAND:
    login: Login creates a JWT token for a given email and sends it to the user.
    login-invitation: LoginInvitation asks the OCM for proof invitation link which can be displayed for scanning by PCM.
    login-with-vc: LoginWithVC receives presentationId and creates a JWT after receiving valid proof object from the OCM.
    keys: Keys returns a list of public keys used for verification of JWT issued by login.

Additional help:
    %[1]s login COMMAND --help
`, os.Args[0])
}
func loginLoginUsage() {
	fmt.Fprintf(os.Stderr, `%[1]s [flags] login login -body JSON

Login creates a JWT token for a given email and sends it to the user.
    -body JSON: 

Example:
    %[1]s login login --body '{
      "email": "ima@streichkuhic.com"
   }'
`, os.Args[0])
}

func loginLoginInvitationUsage() {
	fmt.Fprintf(os.Stderr, `%[1]s [flags] login login-invitation

LoginInvitation asks the OCM for proof invitation link which can be displayed for scanning by PCM.

Example:
    %[1]s login login-invitation
`, os.Args[0])
}

func loginLoginWithVCUsage() {
	fmt.Fprintf(os.Stderr, `%[1]s [flags] login login-with-vc -body JSON

LoginWithVC receives presentationId and creates a JWT after receiving valid proof object from the OCM.
    -body JSON: 

Example:
    %[1]s login login-with-vc --body '{
      "proofRecordId": "9e899887-5c45-409d-b269-c3fb8a1da8aa"
   }'
`, os.Args[0])
}

func loginKeysUsage() {
	fmt.Fprintf(os.Stderr, `%[1]s [flags] login keys

Keys returns a list of public keys used for verification of JWT issued by login.

Example:
    %[1]s login keys
`, os.Args[0])
}
