// Code generated by goa v3.12.3, DO NOT EDIT.
//
// login HTTP client types
//
// Command:
// $ goa gen gitlab.eclipse.org/eclipse/xfsc/tsa/login/design

package client

import (
	login "gitlab.eclipse.org/eclipse/xfsc/tsa/login/gen/login"
	goa "goa.design/goa/v3/pkg"
)

// LoginRequestBody is the type of the "login" service "Login" endpoint HTTP
// request body.
type LoginRequestBody struct {
	Email string `form:"email" json:"email" xml:"email"`
}

// LoginWithVCRequestBody is the type of the "login" service "LoginWithVC"
// endpoint HTTP request body.
type LoginWithVCRequestBody struct {
	ProofRecordID string `form:"proofRecordId" json:"proofRecordId" xml:"proofRecordId"`
}

// LoginResponseBody is the type of the "login" service "Login" endpoint HTTP
// response body.
type LoginResponseBody struct {
	// Status message response.
	Message *string `form:"message,omitempty" json:"message,omitempty" xml:"message,omitempty"`
}

// LoginInvitationResponseBody is the type of the "login" service
// "LoginInvitation" endpoint HTTP response body.
type LoginInvitationResponseBody struct {
	// Identifier of the proof invitation request.
	ProofRecordID *string `form:"proofRecordId,omitempty" json:"proofRecordId,omitempty" xml:"proofRecordId,omitempty"`
	// Link containing the invitation proof request which the PCM can scan to login.
	PresentationMessage *string `form:"presentationMessage,omitempty" json:"presentationMessage,omitempty" xml:"presentationMessage,omitempty"`
	// Shortened URL link which the PCM can scan to login.
	PresentationMessageShort *string `form:"presentationMessageShort,omitempty" json:"presentationMessageShort,omitempty" xml:"presentationMessageShort,omitempty"`
	// Timestamp for when the invitation was created.
	CreatedDate *string `form:"createdDate,omitempty" json:"createdDate,omitempty" xml:"createdDate,omitempty"`
}

// LoginWithVCResponseBody is the type of the "login" service "LoginWithVC"
// endpoint HTTP response body.
type LoginWithVCResponseBody struct {
	// Token in JWT format after successful login.
	Token any `form:"token,omitempty" json:"token,omitempty" xml:"token,omitempty"`
}

// KeysResponseBody is the type of the "login" service "Keys" endpoint HTTP
// response body.
type KeysResponseBody struct {
	// List of public keys.
	Keys []any `form:"keys,omitempty" json:"keys,omitempty" xml:"keys,omitempty"`
}

// NewLoginRequestBody builds the HTTP request body from the payload of the
// "Login" endpoint of the "login" service.
func NewLoginRequestBody(p *login.LoginRequest) *LoginRequestBody {
	body := &LoginRequestBody{
		Email: p.Email,
	}
	return body
}

// NewLoginWithVCRequestBody builds the HTTP request body from the payload of
// the "LoginWithVC" endpoint of the "login" service.
func NewLoginWithVCRequestBody(p *login.LoginVCRequest) *LoginWithVCRequestBody {
	body := &LoginWithVCRequestBody{
		ProofRecordID: p.ProofRecordID,
	}
	return body
}

// NewLoginResultOK builds a "login" service "Login" endpoint result from a
// HTTP "OK" response.
func NewLoginResultOK(body *LoginResponseBody) *login.LoginResult {
	v := &login.LoginResult{
		Message: *body.Message,
	}

	return v
}

// NewLoginInvitationResultOK builds a "login" service "LoginInvitation"
// endpoint result from a HTTP "OK" response.
func NewLoginInvitationResultOK(body *LoginInvitationResponseBody) *login.LoginInvitationResult {
	v := &login.LoginInvitationResult{
		ProofRecordID:            *body.ProofRecordID,
		PresentationMessage:      *body.PresentationMessage,
		PresentationMessageShort: *body.PresentationMessageShort,
		CreatedDate:              *body.CreatedDate,
	}

	return v
}

// NewLoginWithVCLoginVCResultOK builds a "login" service "LoginWithVC"
// endpoint result from a HTTP "OK" response.
func NewLoginWithVCLoginVCResultOK(body *LoginWithVCResponseBody) *login.LoginVCResult {
	v := &login.LoginVCResult{
		Token: body.Token,
	}

	return v
}

// NewKeysResultOK builds a "login" service "Keys" endpoint result from a HTTP
// "OK" response.
func NewKeysResultOK(body *KeysResponseBody) *login.KeysResult {
	v := &login.KeysResult{}
	v.Keys = make([]any, len(body.Keys))
	for i, val := range body.Keys {
		v.Keys[i] = val
	}

	return v
}

// ValidateLoginResponseBody runs the validations defined on LoginResponseBody
func ValidateLoginResponseBody(body *LoginResponseBody) (err error) {
	if body.Message == nil {
		err = goa.MergeErrors(err, goa.MissingFieldError("message", "body"))
	}
	return
}

// ValidateLoginInvitationResponseBody runs the validations defined on
// LoginInvitationResponseBody
func ValidateLoginInvitationResponseBody(body *LoginInvitationResponseBody) (err error) {
	if body.ProofRecordID == nil {
		err = goa.MergeErrors(err, goa.MissingFieldError("proofRecordId", "body"))
	}
	if body.PresentationMessage == nil {
		err = goa.MergeErrors(err, goa.MissingFieldError("presentationMessage", "body"))
	}
	if body.PresentationMessageShort == nil {
		err = goa.MergeErrors(err, goa.MissingFieldError("presentationMessageShort", "body"))
	}
	if body.CreatedDate == nil {
		err = goa.MergeErrors(err, goa.MissingFieldError("createdDate", "body"))
	}
	return
}

// ValidateLoginWithVCResponseBody runs the validations defined on
// LoginWithVCResponseBody
func ValidateLoginWithVCResponseBody(body *LoginWithVCResponseBody) (err error) {
	if body.Token == nil {
		err = goa.MergeErrors(err, goa.MissingFieldError("token", "body"))
	}
	return
}

// ValidateKeysResponseBody runs the validations defined on KeysResponseBody
func ValidateKeysResponseBody(body *KeysResponseBody) (err error) {
	if body.Keys == nil {
		err = goa.MergeErrors(err, goa.MissingFieldError("keys", "body"))
	}
	return
}
