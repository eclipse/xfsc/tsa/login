[![pipeline status](https://gitlab.eclipse.org/eclipse/xfsc/tsa/login/badges/main/pipeline.svg)](https://gitlab.eclipse.org/eclipse/xfsc/tsa/login/-/commits/main)
[![coverage report](https://gitlab.eclipse.org/eclipse/xfsc/tsa/login/badges/main/coverage.svg)](https://gitlab.eclipse.org/eclipse/xfsc/tsa/login/-/commits/main)

# login

Login is an experimental service to test some flows with policies and passwordless login.

It's not needed to deploy this service as part of the TSA deployment 
as no other services depend on it for their functionalities. It may be viewed 
as an example and testing experiment separate from the TSA core services.

### Login Flow Diagram

The login flow starts with the client sending HTTP POST request with valid *email* address to login.
```json
{
	"email":"test@example.com"
}
```

The login service checks with the policy service if the given email is trusted (allowed) to login. 
If so, the login service creates a signed JWT and sends an email to the given mail address with a 
login link containing the JWT as query string parameter.

```mermaid  
flowchart LR
	A([client]) -- POST --> B["/v1/login"] 
	subgraph login flow
		B --1--> C[policy]
		B --2--> D[Create JWT]
		B --3--> E[Send email with JWT link]
	end
```

### Configuration

The login service is configured using the [Configuration File](./internal/config/config.go).
All configurations are expected as Environment variables specified in the
configuration file. For managing the configuration data from ENV variables,
[envconfig library](https://github.com/kelseyhightower/envconfig) is used.


### Login Flow Sequence Diagram

```mermaid
sequenceDiagram
Client ->> Login: POST /v1/login {"email":"test@example.com"}
Login->>Policy: Is email is allowed to login?
Policy->>Login: {"allowed":true}
Login->>Login: make signed JWT
Login->>Email: SMTP: send email with login link to test@example.com
Login->>Client: HTTP 200 OK
```

### Public Keys

The service exposes its public keys for verifying JWT signatures at:

```
http://host:port/keys
```

`GET /keys` yields response like:

```json
{
  "keys": [
    {
      "alg": "RS256",
      "e": "AQAB",
      "kty": "RSA",
      "n": "pqjP9IEYtGdGNAEawn/bGoyM6iHxOvs7yFhbHmOUZGMgiaviv4/IPq2BtZEL82bxO5NZSXNlpS++bG8QCmPdPn5UTdtkH1MYhobqbraPq5EwzmLsOBc74dDNySoYOyPIm8afEHYGZdIYTygvANQa9b+VuRab5COvYw9gMoV6CBIke4VN7pMlZcwDoTO4P4y291tywIeBq6j0X4n4k7k5Gh20NxKrmncyH8gwCW1MX2Q6kuTCJDswxnIc4fudhDQnggDyGBGOzAc9b15zJBgBym54BK9jhbJ5ce2QvA7ELrcKxZOeUKXDaKzlonFPcNTyQ0Nq6IRUWlO0wty5jjcrCRh1rsXUNis/gNnUWXs8DokuOf0A74Qqfs7sipF5EttkTY3D5hnMxfrfidphJHd37nuxQCRBW1oE4zsNtU/RG8Twi+RVmg0DpsCD1f+9MIrnUMjOr98wFoNvp1d4arvSfqui3vhSCmCG5PzTaJ08gJFj1UnSusrp2WcnQmwoUOVZ",
      "use": "sig"
    }
  ]
}
```

### Dependencies and Vendor

The project uses Go modules for managing dependencies and we commit the vendor directory.
When you add/change dependencies, be sure to clean and update the vendor directory before
submitting your Merge Request for review.

```shell
go mod tidy
go mod vendor
```

### Tests and Linters

To execute the units tests for the service go to the root project directory and run:

```shell
go test -race $(go list ./... | grep -v /integration)
```

To run the linters go to the root project directory and run:
```shell
golangci-lint run
```

## Dependencies

[Dependencies](go.mod)

## Deployment

[Helm deployment documentation](deployment/helm/README.md)

## License

[Apache 2.0 license](LICENSE)
