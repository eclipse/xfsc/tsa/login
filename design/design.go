// nolint: revive
package design

import . "goa.design/goa/v3/dsl"

var _ = API("login", func() {
	Title("Login Service")
	Description("Login service return tokens.")
	Server("login", func() {
		Description("Login Service")
		Host("development", func() {
			Description("Local development server")
			URI("http://localhost:8087")
		})
	})
})

var _ = Service("health", func() {
	Description("Health service provides health check endpoints.")

	Method("Liveness", func() {
		Payload(Empty)
		Result(Any)
		HTTP(func() {
			GET("/liveness")
			Response(StatusOK)
		})
	})

	Method("Readiness", func() {
		Payload(Empty)
		Result(Any)
		HTTP(func() {
			GET("/readiness")
			Response(StatusOK)
		})
	})
})

var _ = Service("login", func() {
	Description("Login Service enables authorization for user")

	Method("Login", func() {
		Description("Login creates a JWT token for a given email and sends it to the user.")
		Payload(LoginRequest)
		Result(LoginResult)
		HTTP(func() {
			POST("/v1/login")
			Response(StatusOK)
		})
	})

	Method("LoginInvitation", func() {
		Description("LoginInvitation asks the OCM for proof invitation link which can be displayed for scanning by PCM.")
		Payload(Empty)
		Result(LoginInvitationResult)
		HTTP(func() {
			GET("/v1/login-vc-invitation")
			Response(StatusOK)
		})
	})

	Method("LoginWithVC", func() {
		Description("LoginWithVC receives presentationId and creates a JWT after receiving valid proof object from the OCM.")
		Payload(LoginVCRequest)
		Result(LoginVCResult)
		HTTP(func() {
			POST("/v1/login-vc")
			Response(StatusOK)
		})
	})

	Method("Keys", func() {
		Description("Keys returns a list of public keys used for verification of JWT issued by login.")
		Payload(Empty)
		Result(KeysResult)
		HTTP(func() {
			GET("/keys")
			Response(StatusOK)
		})
	})
})

var _ = Service("openapi", func() {
	Description("The openapi service serves the OpenAPI(v3) definition.")
	Meta("swagger:generate", "false")
	HTTP(func() {
		Path("/swagger-ui")
	})
	Files("/openapi.json", "./gen/http/openapi3.json", func() {
		Description("JSON document containing the OpenAPI(v3) service definition")
	})
	Files("/{*filepath}", "./swagger/")
})
