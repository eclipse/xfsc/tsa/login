// nolint:revive
package design

import . "goa.design/goa/v3/dsl"

var LoginRequest = Type("LoginRequest", func() {
	Field(1, "email", String, func() { Format(FormatEmail) })
	Required("email")
})

var LoginResult = Type("LoginResult", func() {
	Field(1, "message", String, "Status message response.")
	Required("message")
})

var LoginInvitationResult = Type("LoginInvitationResult", func() {
	Field(1, "proofRecordId", String, "Identifier of the proof invitation request.")
	Field(2, "presentationMessage", String, "Link containing the invitation proof request which the PCM can scan to login.")
	Field(3, "presentationMessageShort", String, "Shortened URL link which the PCM can scan to login.")
	Field(4, "createdDate", String, "Timestamp for when the invitation was created.")
	Required("proofRecordId", "presentationMessage", "presentationMessageShort", "createdDate")
})

var LoginVCRequest = Type("LoginVCRequest", func() {
	Field(1, "proofRecordId", String, func() {
		Example("9e899887-5c45-409d-b269-c3fb8a1da8aa")
	})
	Required("proofRecordId")
})

var LoginVCResult = Type("LoginVCResult", func() {
	Field(1, "token", Any, "Token in JWT format after successful login.")
	Required("token")
})

var KeysResult = Type("KeysResult", func() {
	Field(1, "keys", ArrayOf(Any), "List of public keys.")
	Required("keys")
})
