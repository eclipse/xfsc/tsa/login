package config

import "time"

type Config struct {
	HTTP   httpConfig
	Policy policyConfig
	Token  tokenConfig
	Mail   mailConfig
	OCM    ocmConfig

	OCMPollInterval time.Duration `envconfig:"OCM_POLL_INTERVAL" default:"1s"`
	OCMPollTimeout  time.Duration `envconfig:"OCM_POLL_TIMEOUT" default:"1m"`

	AllowedOrigins []string `envconfig:"ALLOWED_ORIGINS"`
	LinkLocation   string   `envconfig:"LINK_LOCATION" required:"true"`
	LogLevel       string   `envconfig:"LOG_LEVEL" default:"INFO"`
}

type httpConfig struct {
	Host         string        `envconfig:"HTTP_HOST"`
	Port         string        `envconfig:"HTTP_PORT" default:"8080"`
	IdleTimeout  time.Duration `envconfig:"HTTP_IDLE_TIMEOUT" default:"180s"`
	ReadTimeout  time.Duration `envconfig:"HTTP_READ_TIMEOUT" default:"10s"`
	WriteTimeout time.Duration `envconfig:"HTTP_WRITE_TIMEOUT" default:"10s"`
}

type policyConfig struct {
	Addr            string `envconfig:"POLICY_ADDR" required:"true"`
	LoginPolicyPath string `envconfig:"POLICY_LOGIN_PATH" required:"true"`
}

type tokenConfig struct {
	PublicKey  string        `envconfig:"PUBLIC_KEY_RSA" required:"true"`
	PrivateKey string        `envconfig:"PRIVATE_KEY_RSA" required:"true"`
	Issuer     string        `envconfig:"TOKEN_ISSUER" required:"true"`
	Audience   string        `envconfig:"TOKEN_AUDIENCE" required:"true"`
	Expiration time.Duration `envconfig:"TOKEN_EXPIRATION" default:"30m"`
}

type mailConfig struct {
	Addr    string `envconfig:"MAIL_ADDR" required:"true"`
	User    string `envconfig:"MAIL_USER"`
	Pass    string `envconfig:"MAIL_PASS"`
	From    string `envconfig:"MAIL_FROM" required:"true"`
	Subject string `envconfig:"MAIL_SUBJECT" default:"Your login link"`
}

type ocmConfig struct {
	Addr              string `envconfig:"OCM_ADDR" required:"true"`
	LoginCredSchemaID string `envconfig:"OCM_LOGIN_SCHEMA_ID" required:"true"`
	LoginCredDefID    string `envconfig:"OCM_LOGIN_CRED_DEF_ID" required:"true"`
}
