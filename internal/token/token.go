package token

import (
	"context"
	"fmt"
	"time"

	"github.com/lestrrat-go/jwx/v2/jwa"
	"github.com/lestrrat-go/jwx/v2/jwk"
	"github.com/lestrrat-go/jwx/v2/jwt"
)

// Token handles the creation and verification of signed JWT tokens.
//
// TODO: Currently it is initialized with a single RSA key pair
// for a PoC implementation, but later it should be extended to
// support multiple keys or a separate signing engine as Hashicorp Vault.
type Token struct {
	pubKey     interface{}
	privKey    interface{}
	issuer     string
	audience   string
	expiration time.Duration
}

// New creates token signer.
// Currently, it works with RSA key pair only.
// Keys are expected as PEM encoded strings.
func New(pubKeyRSA, privKeyRSA string, issuer string, audience string, tokenExpiration time.Duration) (*Token, error) {
	pub, err := jwk.ParseKey([]byte(pubKeyRSA), jwk.WithPEM(true))
	if err != nil {
		return nil, fmt.Errorf("parse public key: %s", err)
	}

	priv, err := jwk.ParseKey([]byte(privKeyRSA), jwk.WithPEM(true))
	if err != nil {
		return nil, fmt.Errorf("parse private key: %s", err)
	}

	return &Token{
		pubKey:     pub,
		privKey:    priv,
		issuer:     issuer,
		audience:   audience,
		expiration: tokenExpiration,
	}, nil
}

func (t *Token) SignedJWT(email string) (string, error) {
	token, err := jwt.NewBuilder().
		Issuer(t.issuer).
		IssuedAt(time.Now()).
		Expiration(time.Now().Add(t.expiration)).
		Subject(email).
		Audience([]string{t.audience}).
		Build()
	if err != nil {
		return "", fmt.Errorf("failed to make token: %s", err)
	}

	signed, err := jwt.Sign(token, jwt.WithKey(jwa.RS256, t.privKey))
	if err != nil {
		return "", fmt.Errorf("failed to sign token: %s", err)
	}

	return string(signed), nil
}

func (t *Token) PublicKeys(ctx context.Context) (keys []interface{}, err error) {
	pub, ok := t.pubKey.(jwk.Key)
	if !ok {
		return nil, fmt.Errorf("unknown public key format")
	}

	// set key alg
	if err := pub.Set("alg", jwa.RS256); err != nil {
		return nil, fmt.Errorf("failed to set key use: %s", err)
	}

	// set key purpose (use)
	if err := pub.Set("use", "sig"); err != nil {
		return nil, fmt.Errorf("failed to set key use: %s", err)
	}

	pubKey, err := pub.AsMap(ctx)
	if err != nil {
		return nil, err
	}

	keys = append(keys, pubKey)

	return keys, nil
}
