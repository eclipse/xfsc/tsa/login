package login

import (
	"context"
	"fmt"
	"time"

	"go.uber.org/zap"

	"gitlab.eclipse.org/eclipse/xfsc/tsa/golib/errors"
	"gitlab.eclipse.org/eclipse/xfsc/tsa/login/gen/login"
	"gitlab.eclipse.org/eclipse/xfsc/tsa/login/internal/clients/ocm"
)

//go:generate counterfeiter . Policy
//go:generate counterfeiter . Token
//go:generate counterfeiter . Email
//go:generate counterfeiter . OCM

type Policy interface {
	TrustedEmail(ctx context.Context, email string) (bool, error)
}

type Token interface {
	SignedJWT(email string) (string, error)
	PublicKeys(ctx context.Context) ([]interface{}, error)
}

type Email interface {
	SendLink(ctx context.Context, email string, link string) error
}

type OCM interface {
	LoginProofInvitation(ctx context.Context, schemaID, credDefID string) (*ocm.ProofInvitation, error)
	LoginProofEmail(ctx context.Context, proofRecordID string) (email string, err error)
}

type Service struct {
	policy            Policy
	token             Token
	email             Email
	linkLocation      string
	ocm               OCM
	pollInterval      time.Duration
	pollTimeout       time.Duration
	loginCredSchemaID string
	loginCredDefID    string
	logger            *zap.Logger
}

func New(policy Policy, token Token, email Email, ocm OCM, linkLocation string,
	pollInterval, pollTimeout time.Duration, loginCredSchemaID, loginCredDefID string, logger *zap.Logger) *Service {
	return &Service{
		policy:            policy,
		token:             token,
		email:             email,
		ocm:               ocm,
		linkLocation:      linkLocation,
		pollInterval:      pollInterval,
		pollTimeout:       pollTimeout,
		loginCredSchemaID: loginCredSchemaID,
		loginCredDefID:    loginCredDefID,
		logger:            logger,
	}
}

// Login creates a JWT token for a given email and sends it to the user.
func (s *Service) Login(ctx context.Context, req *login.LoginRequest) (*login.LoginResult, error) {
	logger := s.logger.With(zap.String("operation", "login"), zap.String("email", req.Email))

	trusted, err := s.policy.TrustedEmail(ctx, req.Email)
	if err != nil {
		logger.Error("policy error", zap.Error(err))
		return nil, errors.New("policy error", err)
	}

	if !trusted {
		logger.Error("untrusted email")
		return nil, errors.New(errors.Forbidden, "untrusted email")
	}

	token, err := s.token.SignedJWT(req.Email)
	if err != nil {
		logger.Error("error creating jwt", zap.Error(err))
		return nil, errors.New("error creating jwt", err)
	}

	loginLink := fmt.Sprintf("%s/auth/%s", s.linkLocation, token)

	if err := s.email.SendLink(ctx, req.Email, loginLink); err != nil {
		logger.Error("error sending email", zap.Error(err))
		return nil, errors.New("error sending email", err)
	}

	logger.Debug("login link is sent")

	return &login.LoginResult{Message: "login link is sent"}, nil
}

// LoginInvitation asks the OCM for proof invitation link which can be
// displayed for scanning by PCM.
func (s *Service) LoginInvitation(ctx context.Context) (*login.LoginInvitationResult, error) {
	logger := s.logger.With(zap.String("operation", "loginInvitation"))

	invitation, err := s.ocm.LoginProofInvitation(ctx, s.loginCredSchemaID, s.loginCredDefID)
	if err != nil {
		logger.Error("error getting login invitation", zap.Error(err))
		return nil, errors.New("error getting login invitation", err)
	}

	return &login.LoginInvitationResult{
		ProofRecordID:            invitation.ProofRecordID,
		PresentationMessage:      invitation.PresentationMessage,
		PresentationMessageShort: invitation.PresentationMessageShort,
		CreatedDate:              invitation.CreatedDate,
	}, nil
}

// LoginWithVC receives proofRecordID and creates a JWT after receiving valid
// proof object with user email from the OCM.
func (s *Service) LoginWithVC(ctx context.Context, req *login.LoginVCRequest) (*login.LoginVCResult, error) {
	logger := s.logger.With(zap.String("operation", "loginWithVC"), zap.String("proofRecordID", req.ProofRecordID))

	email, err := s.loginProofEmail(ctx, req.ProofRecordID)
	if err != nil {
		logger.Error("error getting login proof result", zap.Error(err))
		return nil, errors.New("error getting login proof result", err)
	}

	token, err := s.token.SignedJWT(email)
	if err != nil {
		logger.Error("error creating jwt", zap.Error(err))
		return nil, errors.New("error creating jwt", err)
	}

	logger.Debug("login with VC is successful")

	return &login.LoginVCResult{Token: token}, nil
}

// Keys returns a list of public keys used for verification of JWT issued by
// login.
func (s *Service) Keys(ctx context.Context) (res *login.KeysResult, err error) {
	keys, err := s.token.PublicKeys(ctx)
	if err != nil {
		s.logger.Error("failed to get public keys", zap.Error(err))
		return nil, errors.New("failed to get public keys", err)
	}

	return &login.KeysResult{Keys: keys}, nil
}

// loginProofEmail continuously polls the OCM for login proof result and the email
// claim which should be part of the response object. It handles context cancellation
// signals and does the polling on a preconfigured time interval.
func (s *Service) loginProofEmail(ctx context.Context, proofRecordID string) (string, error) {
	emailAfterProof := make(chan string, 1)
	errAfterProof := make(chan error, 1)
	timeout := time.After(s.pollTimeout)
	go func(ctx context.Context) {
		for {
			email, err := s.ocm.LoginProofEmail(ctx, proofRecordID)
			if err != nil {
				errAfterProof <- err
				return
			}

			if email != "" {
				emailAfterProof <- email
				return
			}

			select {
			case <-ctx.Done():
				return
			case <-timeout:
				errAfterProof <- errors.New(errors.Timeout, fmt.Sprintf("%s timeout for get login proof email", s.pollTimeout))
				return
			case <-time.After(s.pollInterval):
				// try again after poll interval
			}
		}
	}(ctx)

	select {
	case <-ctx.Done():
		return "", ctx.Err()
	case err := <-errAfterProof:
		return "", err
	case email := <-emailAfterProof:
		return email, nil
	}
}
