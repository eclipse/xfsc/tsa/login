package login_test

import (
	"context"
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"go.uber.org/zap"

	"gitlab.eclipse.org/eclipse/xfsc/tsa/golib/errors"
	goalogin "gitlab.eclipse.org/eclipse/xfsc/tsa/login/gen/login"
	"gitlab.eclipse.org/eclipse/xfsc/tsa/login/internal/service/login"
	"gitlab.eclipse.org/eclipse/xfsc/tsa/login/internal/service/login/loginfakes"
)

func TestService_Login(t *testing.T) {
	tests := []struct {
		name   string
		policy login.Policy
		token  login.Token
		email  login.Email
		ocm    login.OCM

		errtext   string
		errorKind errors.Kind
		response  *goalogin.LoginResult
	}{
		{
			name: "Policy Service response with error",
			policy: &loginfakes.FakePolicy{TrustedEmailStub: func(ctx context.Context, s string) (bool, error) {
				return false, errors.New("some error", errors.Unknown)
			}},

			errtext: "policy error",
		},
		{
			name: "Policy service email is not authorized",
			policy: &loginfakes.FakePolicy{TrustedEmailStub: func(ctx context.Context, s string) (bool, error) {
				return false, nil
			}},

			errtext:   "untrusted email",
			errorKind: errors.Forbidden,
		},
		{
			name: "Token failed with creating jwt",
			policy: &loginfakes.FakePolicy{TrustedEmailStub: func(ctx context.Context, s string) (bool, error) {
				return true, nil
			}},
			token: &loginfakes.FakeToken{SignedJWTStub: func(s string) (string, error) {
				return "", errors.New("some error")
			}},

			errtext: "error creating jwt",
		},
		{
			name: "Email service fail sending the email",
			policy: &loginfakes.FakePolicy{TrustedEmailStub: func(ctx context.Context, s string) (bool, error) {
				return true, nil
			}},
			token: &loginfakes.FakeToken{SignedJWTStub: func(s string) (string, error) {
				return "token", nil
			}},
			email: &loginfakes.FakeEmail{SendLinkStub: func(ctx context.Context, s1, s2 string) error {
				return errors.New("some error")
			}},

			errtext: "error sending email",
		},
		{
			name: "Login service sending login link",
			policy: &loginfakes.FakePolicy{TrustedEmailStub: func(ctx context.Context, s string) (bool, error) {
				return true, nil
			}},
			token: &loginfakes.FakeToken{SignedJWTStub: func(s string) (string, error) {
				return "token", nil
			}},
			email: &loginfakes.FakeEmail{SendLinkStub: func(ctx context.Context, s1, s2 string) error {
				return nil
			}},

			response: &goalogin.LoginResult{Message: "login link is sent"},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			login := login.New(
				test.policy, test.token, test.email, nil,
				"http://example.com", 0, 0,
				"", "", zap.NewNop(),
			)

			resp, err := login.Login(context.Background(), &goalogin.LoginRequest{Email: "test@email.com"})
			if test.errtext != "" {
				assert.Nil(t, resp)
				assert.NotNil(t, err)
				assert.Contains(t, err.Error(), test.errtext)
				assert.True(t, errors.Is(test.errorKind, err))
			} else {
				assert.Nil(t, err)
				assert.NotNil(t, resp)
				assert.Equal(t, test.response, resp)
			}
		})
	}
}

func TestService_LoginWithVC(t *testing.T) {
	tests := []struct {
		name         string
		token        login.Token
		ocm          login.OCM
		pollInterval time.Duration
		pollTimeout  time.Duration

		errtext  string
		response *goalogin.LoginVCResult
	}{
		{
			name: "ocm returns an error",
			ocm: &loginfakes.FakeOCM{
				LoginProofEmailStub: func(ctx context.Context, presentationID string) (string, error) {
					return "", fmt.Errorf("some error")
				},
			},
			pollInterval: 100 * time.Millisecond,
			pollTimeout:  1 * time.Second,
			errtext:      "some error",
		},
		{
			name: "polling timeout period is reached without successful login response from ocm",
			ocm: &loginfakes.FakeOCM{
				LoginProofEmailStub: func(ctx context.Context, presentationID string) (string, error) {
					return "", nil
				},
			},
			pollInterval: 100 * time.Millisecond,
			pollTimeout:  1 * time.Second,

			errtext: "1s timeout for get login proof email",
		},
		{
			name: "ocm returns login proof email, but token generation fails",
			ocm: &loginfakes.FakeOCM{
				LoginProofEmailStub: func(ctx context.Context, presentationID string) (string, error) {
					return "hello@vereign.com", nil
				},
			},
			token: &loginfakes.FakeToken{
				SignedJWTStub: func(email string) (string, error) {
					return "", fmt.Errorf("error making jwt")
				},
			},
			pollInterval: 100 * time.Millisecond,
			pollTimeout:  1 * time.Second,
			errtext:      "error making jwt",
		},
		{
			name: "ocm returns login proof email and token is generated",
			ocm: &loginfakes.FakeOCM{
				LoginProofEmailStub: func(ctx context.Context, presentationID string) (string, error) {
					return "hello@vereign.com", nil
				},
			},
			token: &loginfakes.FakeToken{
				SignedJWTStub: func(email string) (string, error) {
					return "jwt", nil
				},
			},
			pollInterval: 100 * time.Millisecond,
			pollTimeout:  1 * time.Second,
			response:     &goalogin.LoginVCResult{Token: "jwt"},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			login := login.New(
				nil,
				test.token,
				nil,
				test.ocm,
				"",
				test.pollInterval,
				test.pollTimeout,
				"", "",
				zap.NewNop(),
			)

			resp, err := login.LoginWithVC(
				context.Background(),
				&goalogin.LoginVCRequest{ProofRecordID: "aecb69be-026a-475b-b7f1-4ab91d1a807e"},
			)

			if test.errtext != "" {
				assert.Nil(t, resp)
				assert.Error(t, err)
				assert.Contains(t, err.Error(), test.errtext)
			} else {
				assert.NoError(t, err)
				assert.NotNil(t, resp)
				assert.Equal(t, test.response, resp)
			}
		})
	}
}
