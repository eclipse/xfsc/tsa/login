// Code generated by counterfeiter. DO NOT EDIT.
package loginfakes

import (
	"context"
	"sync"

	"gitlab.eclipse.org/eclipse/xfsc/tsa/login/internal/service/login"
)

type FakeEmail struct {
	SendLinkStub        func(context.Context, string, string) error
	sendLinkMutex       sync.RWMutex
	sendLinkArgsForCall []struct {
		arg1 context.Context
		arg2 string
		arg3 string
	}
	sendLinkReturns struct {
		result1 error
	}
	sendLinkReturnsOnCall map[int]struct {
		result1 error
	}
	invocations      map[string][][]interface{}
	invocationsMutex sync.RWMutex
}

func (fake *FakeEmail) SendLink(arg1 context.Context, arg2 string, arg3 string) error {
	fake.sendLinkMutex.Lock()
	ret, specificReturn := fake.sendLinkReturnsOnCall[len(fake.sendLinkArgsForCall)]
	fake.sendLinkArgsForCall = append(fake.sendLinkArgsForCall, struct {
		arg1 context.Context
		arg2 string
		arg3 string
	}{arg1, arg2, arg3})
	stub := fake.SendLinkStub
	fakeReturns := fake.sendLinkReturns
	fake.recordInvocation("SendLink", []interface{}{arg1, arg2, arg3})
	fake.sendLinkMutex.Unlock()
	if stub != nil {
		return stub(arg1, arg2, arg3)
	}
	if specificReturn {
		return ret.result1
	}
	return fakeReturns.result1
}

func (fake *FakeEmail) SendLinkCallCount() int {
	fake.sendLinkMutex.RLock()
	defer fake.sendLinkMutex.RUnlock()
	return len(fake.sendLinkArgsForCall)
}

func (fake *FakeEmail) SendLinkCalls(stub func(context.Context, string, string) error) {
	fake.sendLinkMutex.Lock()
	defer fake.sendLinkMutex.Unlock()
	fake.SendLinkStub = stub
}

func (fake *FakeEmail) SendLinkArgsForCall(i int) (context.Context, string, string) {
	fake.sendLinkMutex.RLock()
	defer fake.sendLinkMutex.RUnlock()
	argsForCall := fake.sendLinkArgsForCall[i]
	return argsForCall.arg1, argsForCall.arg2, argsForCall.arg3
}

func (fake *FakeEmail) SendLinkReturns(result1 error) {
	fake.sendLinkMutex.Lock()
	defer fake.sendLinkMutex.Unlock()
	fake.SendLinkStub = nil
	fake.sendLinkReturns = struct {
		result1 error
	}{result1}
}

func (fake *FakeEmail) SendLinkReturnsOnCall(i int, result1 error) {
	fake.sendLinkMutex.Lock()
	defer fake.sendLinkMutex.Unlock()
	fake.SendLinkStub = nil
	if fake.sendLinkReturnsOnCall == nil {
		fake.sendLinkReturnsOnCall = make(map[int]struct {
			result1 error
		})
	}
	fake.sendLinkReturnsOnCall[i] = struct {
		result1 error
	}{result1}
}

func (fake *FakeEmail) Invocations() map[string][][]interface{} {
	fake.invocationsMutex.RLock()
	defer fake.invocationsMutex.RUnlock()
	fake.sendLinkMutex.RLock()
	defer fake.sendLinkMutex.RUnlock()
	copiedInvocations := map[string][][]interface{}{}
	for key, value := range fake.invocations {
		copiedInvocations[key] = value
	}
	return copiedInvocations
}

func (fake *FakeEmail) recordInvocation(key string, args []interface{}) {
	fake.invocationsMutex.Lock()
	defer fake.invocationsMutex.Unlock()
	if fake.invocations == nil {
		fake.invocations = map[string][][]interface{}{}
	}
	if fake.invocations[key] == nil {
		fake.invocations[key] = [][]interface{}{}
	}
	fake.invocations[key] = append(fake.invocations[key], args)
}

var _ login.Email = new(FakeEmail)
