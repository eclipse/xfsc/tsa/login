package health

import "context"

type Service struct{}

func New() *Service {
	return &Service{}
}

func (s *Service) Liveness(_ context.Context) (interface{}, error) {
	return map[string]interface{}{"status": "up"}, nil
}

func (s *Service) Readiness(_ context.Context) (interface{}, error) {
	return map[string]interface{}{"status": "up"}, nil
}
