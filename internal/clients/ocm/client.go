package ocm

import (
	"context"
	"net/http"

	"gitlab.eclipse.org/eclipse/xfsc/tsa/golib/ocm"
)

type ProofInvitation struct {
	ProofRecordID            string `json:"proofRecordId"`
	PresentationMessage      string `json:"presentationMessage"`
	PresentationMessageShort string `json:"presentationMessageShort"`
	CreatedDate              string `json:"createdDate"`
}

type Client struct {
	ocm *ocm.Client
}

func New(addr string, httpClient *http.Client) *Client {
	proofManagerAddr := addr + "/proof"

	ocmClient := ocm.New(proofManagerAddr, ocm.WithHTTPClient(httpClient))

	return &Client{ocm: ocmClient}
}

func (c *Client) LoginProofInvitation(ctx context.Context, schemaID, credDefID string) (*ProofInvitation, error) {
	reqPayload := map[string]interface{}{
		"attributes": []map[string]interface{}{
			{
				"schemaId":        schemaID,
				"credentialDefId": credDefID,
				"attributeName":   "email",
				"value":           "",
				"condition":       "",
			},
		},
		"options": map[string]interface{}{
			"type": "Aries 1.0",
		},
	}

	proofInvitation, err := c.ocm.SendOutOfBandRequest(ctx, reqPayload)
	if err != nil {
		return nil, err
	}

	return &ProofInvitation{
		ProofRecordID:            proofInvitation.Data.ProofRecordID,
		PresentationMessage:      proofInvitation.Data.PresentationMessage,
		PresentationMessageShort: proofInvitation.Data.PresentationMessageShort,
		CreatedDate:              proofInvitation.Data.CreatedDate,
	}, nil
}

func (c *Client) LoginProofEmail(ctx context.Context, proofRecordID string) (email string, err error) {
	result, err := c.ocm.GetLoginProofResult(ctx, proofRecordID)
	if err != nil {
		return "", err
	}

	if len(result.Data.Presentations) == 0 {
		return "", nil
	}

	// TODO: what happens if there are multiple presentations, is it possible in this situation?
	first := result.Data.Presentations[0]
	claimEmail, ok := first.Claims["email"]
	if !ok {
		return "", nil
	}

	email, _ = claimEmail.(string)

	return email, nil
}
