package email

import (
	"context"
	"fmt"
	"net"
	"strconv"

	"github.com/go-gomail/gomail"
)

type Client struct {
	host    string
	port    int
	user    string
	pass    string
	from    string
	subject string
}

func New(addr string, user, pass string, from string, subject string) (*Client, error) {
	host, port, err := net.SplitHostPort(addr)
	if err != nil {
		return nil, err
	}

	portInt, err := strconv.Atoi(port)
	if err != nil {
		return nil, fmt.Errorf("cannot convert port to integer: %v", err)
	}

	return &Client{
		host:    host,
		port:    portInt,
		user:    user,
		pass:    pass,
		from:    from,
		subject: subject,
	}, nil
}

func (c *Client) SendLink(_ context.Context, email string, link string) error {
	msg := buildMessage(c.from, email, c.subject, link)
	d := gomail.NewDialer(c.host, c.port, c.user, c.pass)
	if err := d.DialAndSend(msg); err != nil {
		return fmt.Errorf("failed to send email: %s", err)
	}

	return nil
}

func buildMessage(from string, to string, subject, message string) *gomail.Message {
	m := gomail.NewMessage()
	m.SetHeader("From", from)
	m.SetHeader("To", to)
	m.SetHeader("Subject", subject)
	m.SetBody("text/plain", message)
	return m
}
