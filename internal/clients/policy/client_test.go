package policy_test

import (
	"context"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.eclipse.org/eclipse/xfsc/tsa/golib/errors"
	"gitlab.eclipse.org/eclipse/xfsc/tsa/login/internal/clients/policy"
)

func TestClient_TrustedEmail(t *testing.T) {
	tests := []struct {
		name    string
		email   string
		handler http.HandlerFunc

		errtext string
		errKind errors.Kind
		trusted bool
	}{
		{
			name:  "policy service returns 404 Not Found error",
			email: "test@abv.bg",
			handler: func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(http.StatusNotFound)
				_, _ = w.Write([]byte(`{"error":{"bad error"}}`))
			},
			errtext: "bad error",
			errKind: errors.NotFound,
			trusted: false,
		},
		{
			name:  "policy service allows the email to login",
			email: "test@abv.bg",
			handler: func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(http.StatusOK)
				_, _ = w.Write([]byte(`{"allow":true}`))
			},
			trusted: true,
		},
		{
			name:  "policy service returns invalid response",
			email: "test@abv.bg",
			handler: func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(http.StatusOK)
				_, _ = w.Write([]byte(`{"allow":"not correct response"}`))
			},
			errtext: `policy client decoding response: `,
			errKind: -1,
			trusted: false,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			srv := httptest.NewServer(test.handler)
			client := policy.New(srv.URL, "/loginPath")
			trusted, err := client.TrustedEmail(context.Background(), test.email)
			if test.errtext != "" {
				assert.Error(t, err)
				assert.Contains(t, err.Error(), test.errtext)
				if test.errKind != -1 {
					assert.True(t, errors.Is(test.errKind, err))
				}
				assert.False(t, trusted)
			} else {
				assert.NotNil(t, trusted)
				assert.Equal(t, test.trusted, trusted)
			}
		})
	}
}
