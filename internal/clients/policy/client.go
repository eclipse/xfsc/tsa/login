package policy

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"

	"gitlab.eclipse.org/eclipse/xfsc/tsa/golib/errors"
)

type Client struct {
	addr            string
	loginPolicyPath string
	httpClient      *http.Client
}

func New(addr string, loginPolicyPath string, options ...Option) *Client {
	client := &Client{
		addr:            addr,
		loginPolicyPath: loginPolicyPath,
		httpClient:      http.DefaultClient,
	}

	for _, opt := range options {
		opt(client)
	}

	return client
}

func (c *Client) TrustedEmail(ctx context.Context, email string) (bool, error) {
	req := &TrustedEmailRequest{Email: email}
	reqBytes, err := json.Marshal(req)
	if err != nil {
		return false, fmt.Errorf("policy client marshal error: %w", err)
	}

	path := c.addr + "/policy" + c.loginPolicyPath

	request, err := http.NewRequestWithContext(ctx, http.MethodPost, path, bytes.NewReader(reqBytes))
	if err != nil {
		return false, fmt.Errorf("policy client error: %w", err)
	}

	resp, err := c.httpClient.Do(request)
	if err != nil {
		return false, fmt.Errorf("policy client error: POST %s: %w", path, err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return false, errors.New(errors.GetKind(resp.StatusCode), getErrorBody(resp))
	}

	var response TrustedEmailResponse
	if err := json.NewDecoder(resp.Body).Decode(&response); err != nil {
		return false, fmt.Errorf("policy client decoding response: %w", err)
	}

	return response.Allow, nil
}

type TrustedEmailRequest struct {
	Email string `json:"email"`
}

type TrustedEmailResponse struct {
	Allow bool `json:"allow"`
}

func getErrorBody(resp *http.Response) string {
	body, err := io.ReadAll(io.LimitReader(resp.Body, 2<<20))
	if err != nil {
		return ""
	}
	return string(body)
}
